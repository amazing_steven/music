import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    headNav : '1',
    head: {
			toggle: false,
			title: '',
			style: {'background': 'rgba(43,162,251,0)'}
		},
  },
  getters:{
    headNav: state => state.headNav,
    head: state => state.head,
  },
  mutations: {
    setHeadNav(state,nav){
      state.headNav = nav;
    },
    showHead(state, flag){
			state.head.toggle = flag
		},
    setHeadTitle(state, title){
			state.head.title = title
		},
		setHeadStyle(state, style){
			state.head.style = style
    },
    resetHeadStyle: state => {
			state.head.style = {'background': 'rgba(43,162,251,0)'}
		},
  },
  actions: {}
});
