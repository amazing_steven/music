import Vue from "vue";
import Router from "vue-router";
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import ('./views/Home.vue')
    },
    {
      path: "/rank",
      name: "rank",
      component: () =>
        import("./views/rank/main.vue")
    },
    {
      path: "/rank/list/:id",
      name: "ranklist",
      component: () =>
        import("./views/rank/list.vue")
    },
    {
      path: "/plist",
      name: "plist",
      component: () =>
        import("./views/plist/main.vue")
    },
    {
      path: "/plist/list/:id",
      name: "plistlist",
      component: () =>
        import("./views/plist/list.vue")
    },
    {
      path: "/singer",
      name: "singer",
      component: () =>
        import("./views/singer/home.vue")
    },
    {
      path: "/singer/list/:id",
      name: "singerlist",
      component: () =>
        import("./views/singer/list.vue")
    },
    {
      path: "/singer/musiclist/:id",
      name: "musciclist",
      component: () =>
        import("./views/singer/musiclist.vue")
    }
  ]
});
