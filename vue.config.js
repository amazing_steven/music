module.exports = {
    baseUrl: './',
    css: {
      loaderOptions: {
        sass: {
          data: `
            @import "@/assets/styles/sass/_variable.scss";
          `
        }
      }
    },
    devServer: {
        https: false, // https:{type:Boolean}
        open: false, //配置自动启动浏览器
        proxy: {
          '/proxy': {
              target: 'http://m.kugou.com/', 
              ws: true, 
              changeOrigin: true, 
              pathRewrite: {
                  '^/proxy': ''
              }
          },
        } 
    }
  }